# Mapa 1
```plantuml
@startmindmap
*[#Pink] Computadoras Electrónicas
	*[#Orange] siglo XX
		*[#GREEN] Las computadoras de propósitoespecífico \nque funcionaban para una sola cosa \nempezaron a hacer todo un éxito en el \ngobierno y los negocios ayudando en las \ntareas manuales y a veces hasta \nreemplazándolas completamente 
		*[#GREEN] La humanidad siguió creciendo y para la primera \nmitad del siglo 20 la población prácticamente se \nhabía duplicado 
			*[#Yellow] *Cerca de 70 millones de personas fueron \nmovilizadas por la primera guerra mundial. \n*La segunda involucró a más de 100 millones de \npersonas  \n*El comercio mundial y las redes de tránsito se \ninterconectaron como nunca antes la ciencia \n*La ingeniería alcanzaron nuevos límites \n*Ya se pensaba en visitar otros planetas. 
			*[#Yellow] Ocurrió una explosión en la complejidad en la \nburocracia y por consecuencia la cantidad de datos \nlo cual aumentó la necesidad de automatización y \nde computación en poco tiempo esos dispositivos \nelectromecánicos que al principio eran de tamaño \nde gabinetes, empezaron a crecer hasta convertirse \nen monstruos gigantes que ocupaban toda una \nhabitación y que también eran de costoso \nmantenimiento llegando a ese punto era inevitable \ny necesaria una innovación
	*[#Orange] Harvard mark I
		*[#GREEN] Una de las computadoras electromecánicas más grandes \nconstruidas esta fue construida por IBM en el año de 1944 \ndurante la segunda guerra mundial para los aliados
			*[#yellow] Tenía 765 mil componentes 80 kilómetros de cable un eje de 15 \nmetros que atravesaba toda la estructura para mantener en \nsincronía el dispositivo y utilizaba un motor de 5 caballos de \nfuerza. 
			*[#yellow] Esta hacia tres sumas o restas por segundo la multiplicación \nllevaba seis segundos la división 15 las operaciones complejas o \nlas funciones trigonométricas llevan más de un minuto
				*[#lightgreen] Contaba aproximadamente con 3500 reales esto provocaba que \npor pura estadística fallas en un relé al día y ya se puede ver el \nproblema cuando se tenían que hacer cálculos complejos que \nllevaban varios días 
				*[#lightgreen] Un problema más, como eran máquinas inmensas oscuras y \ncalientes atraían insectos
					*[#Lightblue] En septiembre de 1947 y después de investigar fallos en la Mark \nlos operadores de estas sacan una polilla muerta de uno de los \nrelés de la máquina entonces Gray Cooper que fue una \ncientífica de computación que trabajó en las Mark se dijo desde \nentonces cuando algo salía mal con una computadora decíamos \nque tenían bichos.
						*[#white] Llegado a este momento era más que obvio que se necesitaba una \nalternativa más rápida y confiable que los relés
				*[#Lightgreen] Uno de los usos que tuvo esta computadora fue hacer simulaciones \ncálculos para el proyecto Manhattan dicho \nproyecto fue el que desarrolló la bomba atómica.
			*[#yellow]  Relé
				*[#lightgreen] Fue la piedra angular de estas bestias electromecánicas, era  un \ninterruptor mecánico controlado eléctricamente 
					*[#lightblue] El problema con los reales aparte de la velocidad era la vida útil \ncualquier componente que tenga partes móviles es más \nsusceptible al daño por el uso y por el tiempo algunos \ncomponentes se averían completamente otros se vuelven más \nlentos por lo cual no son confiables
						*[#white] Un buen relé en 1940 tenía la capacidad de cambiar de estados \n150 veces por segundo esto mucho pero no es lo suficiente \ncuando se tienen que trabajar con problemas grandes y complejos 
			*[#yellow] Valvula Termoionica
				*[#lightgreen] La alternativa a los relé ya existía, en 1904 un físico inglés \nllamado John Ambrose Fleming desarrolló un nuevo \ncomponente eléctrico llamado válvula termo iónica.
					*[#lightblue] Se encontraba formada por un filamento y dos electrodos que \nestán situados dentro de un bulbo de cristal sellado cuando el \nfilamento se enciende y se calientapermite el flujo de \nelectrones desde el anodo  hacia el cátodo este proceso es \nllamado emisión térmica y permite el flujo de la corriente en \nuna sola dirección lo cual hoy es conocido como diodo. 
						*[#white] Ahora lo que se necesitaba era un interruptor eléctrico \nafortunadamente un poco después en 1906 el inventor \namericano Lee de Forest agregó un tercer electrodo llamado \nelectrodo de control .
			*[#yellow] Electrodo de control
				*[#lightgreen] El electrodo de control estaba situado entre el ánodo y el cátodo \ndel diseño de Fleming, al aplicar una carga positiva o negativa \nen el electrodo de control permite el flujo o detienen la \ncorriente respectivamente logrando así la misma funcionalidad \nque los relés pero sin el problema de las partes móviles \nteniendo más confiabilidad ya que se dañaban menos en el uso \ny también mayor velocidad. 
					*[#lightblue] Estos tubos tenían la capacidad de cambiar de estado miles de \nveces por segundo, estos tubos triodos fueron la base para: \n*La radio  \n*Teléfonos de larga distancia \ny muchos otros dispositivos electrónicos por cerca de 50 años sin embargo estos \ntubos de vacío no eran perfectos eran frágiles y se podían \nquemar como los focos o bulbos de luz.	
						*[#white] Los bulbos o tubos de vacio eran costosos las radios normalmente \nllevaban solo uno sin embargo las computadoras podrían \nrequerir cientos de miles. Afortunadamente para la década en \n1940 se volvieron más accesibles e hicieron factibles \ncomputadoras en base a este componente. 
	*[#Orange] Computadoras electronicas
		*[#green] El hecho de reemplazar a los relés por los tubos de vacío supuso el \nhito que dejó atrás a las computadoras electromecánicas e \ninició la era de las computadoras electrónica el primer uso a \ngran escala de los tubos de vacío en la computación fue en la \nColosus Mark I diseñada por el ingeniero Tommy flowers que \nfue finalizada en diciembre de 1943.
			*[#yellow] Colossus fue instalada en el parque bletchley en el Reino unido y \nayudaba a decodificar las comunicaciones nazis, dos años antes \nAlan Turing había creado en el mismo lugar un dispositivo \nelectromecánico llamado the bomb que fue diseñado para \ndescifrar el código enigma nazi pero técnicamente de bomb no \n era una computadora. 
				*[#lightgreen] La primera versión de colossus tenía 1600 tubos de vacío y en \ntotal se construyeron 10 para ayudar en la decodificación.
		*[#green] Primeras computadoras electronicas
			*[#yellow] Colossus es conocida como la primera computadora electrónica \nprogramable, la configuración o la programación de la \ncomputadora se realizaba mediante la conexión de cientos de \ncables en un tablero similar a los antiguos tableros de \nconmutación de los teléfonos
				*[#lightgreen] Luego the colossus vino la ENIAC siglas de calculadora integradora \nnumérica electrónica que fue construida en 1946 en la \nuniversidad de pensilvania fue diseñada por John Mauchly y Jay \nPresper y fue primera computadora electrónica programable de \npropósito general.
					*[#lightblue] ENIAC que podría realizar 5000 sumas y restas de 10 dígitos por \nsegundo mucho más que todos los dispositivos creados hasta el \nmomento opera durante 10 años y se calcula que hizo más \noperaciones aritméticas que toda la humanidad hasta ese \nmomento, pero debido a las constantes fallas de los tubos de \nvacío ENIAC era solamente operacional durante la mitad del día \nantes de dañarse.
		*[#green] Para la década de 1950 incluso la computación basada en tubos \nde vacío estaba llegando a sus límites se necesitaría un \nradicalmente nuevo interruptor eléctrico para poder reducir los \ncostes y el tamaño así como aumentar la velocidad y confiabilidad.
			*[#yellow] El transistor
				*[#lightgreen] En 1947 los científicos de los laboratorios Bell crearon el transistor \ny con la invención de este se inició toda una nueva era de la \ncomputación.
					*[#lightblue] Un transistor cumple la misma función que un relé o un tubo de \nvacío es decir un interruptor eléctrico los transistores están \nhechos de un elemento el llamado silicio que en su estado puro es \naislante es decir no permite el paso de corriente eléctrica pero \ngracias a un paso llamado dopaje se le agregan otros elementos al \nsilicio los cuales permiten la conducción de la corriente eléctrica.
					*[#lightblue] El primer transistor en los laboratorios Bells se mostraba muy \nprometedor ya que podría cambiar de estados unas 10.000 \nveces por segundo y al contrario de los tubos de vacío que eran \nfabricados de vidrio y de componentes internos frágiles los \ntransistores eran sólidos. 
						*[#white] Gracias a esto se pudieron construir computadoras mucho más \nbaratas como por ejemplo IBM 608 que fue lanzada en 1957 , la \nprimera computadora disponible comercialmente basada \nenteramente en transistores podría realizar cerca de 4500 sumas \n80 divisiones o multiplicaciones por segundo.
						*[#white] IBM empezó a usar transistores en todos sus productos acercando \nde esta forma las computadoras a las oficinas y en los hogares \nhoy en día los transistores utilizados en las computadoras \ntienen un tamaño menor a 50 nanómetros. 
						*[#white] Dependiendo del elemento que se le agrega al silicio éste puede \ntener exceso o defecto de electrones el transistor está construido \ncon tres capas de estos tipos de silicio en forma de sándwich cada \ncapa representa un conector y estos conectores son llamados: \n*Colector \n*Emisor \n*Base 
					*[#lightblue] Cuando hay un flujo de corriente entre la base y el emisor esto \ntambién permite que exista un flujo de corriente entre el \nemisor y el colector cumpliendo de esta forma el objetivo de \ncomportarse como interruptor eléctrico.

@endmindmap
```
# Mapa 2
```plantuml
@startmindmap
*[#Pink] Arquitectura Von Neumann y Arquitectura Harvard
	
@endmindmap
```
# Mapa 3
```plantuml
@startmindmap
*[#Pink] Basura Electrónica
	
@endmindmap